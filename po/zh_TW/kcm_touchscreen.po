# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Kisaragi Hiu <mail@kisaragi-hiu.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-09 02:14+0000\n"
"PO-Revision-Date: 2022-12-26 04:00+0900\n"
"Last-Translator: Kisaragi Hiu <mail@kisaragi-hiu.com>\n"
"Language-Team: Traditional Chinese <zh-l10n@linux.org.tw>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 22.12.0\n"

#: kcmtouchscreen.cpp:44
#, kde-format
msgid "Automatic"
msgstr "自動"

#: kcmtouchscreen.cpp:50
#, kde-format
msgctxt "model - (x,y widthxheight)"
msgid "%1 - (%2,%3 %4×%5)"
msgstr "%1 - (%2,%3 %4×%5)"

#: ui/main.qml:28
#, kde-format
msgid "No touchscreens found"
msgstr "找不到觸控螢幕"

#: ui/main.qml:44
#, kde-format
msgctxt "@label:listbox The device we are configuring"
msgid "Device:"
msgstr "裝置："

#: ui/main.qml:57
#, kde-format
msgid "Enabled:"
msgstr "已啟用："

#: ui/main.qml:65
#, kde-format
msgid "Target display:"
msgstr "目標顯示器："
