# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# SPDX-FileCopyrightText: 2014, 2017, 2018, 2019, 2020, 2021, 2022, 2023 Eloy Cuadra <ecuadra@eloihr.net>
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-04 12:20+0000\n"
"PO-Revision-Date: 2023-11-07 21:46+0100\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.08.2\n"

#: ui/main.qml:24
#, kde-format
msgid ""
"The system must be restarted before changes to the middle-click paste "
"setting can take effect."
msgstr ""
"Debe reiniciar el sistema para que los cambios de la preferencia de pegar "
"con un clic central tengan efecto."

#: ui/main.qml:30
#, kde-format
msgid "Restart"
msgstr "Reiniciar"

#: ui/main.qml:47
#, kde-format
msgid "Visual behavior:"
msgstr "Comportamiento visual:"

#. i18n: ectx: label, entry (delay), group (PlasmaToolTips)
#: ui/main.qml:48 workspaceoptions_plasmasettings.kcfg:9
#, kde-format
msgid "Display informational tooltips on mouse hover"
msgstr "Mostrar ayudas emergentes informativas al situar el ratón encima"

#. i18n: ectx: label, entry (osdEnabled), group (OSD)
#: ui/main.qml:59 workspaceoptions_plasmasettings.kcfg:15
#, kde-format
msgid "Display visual feedback for status changes"
msgstr "Mostrar información visual para cambios de estado"

#: ui/main.qml:77
#, kde-format
msgid "Animation speed:"
msgstr "Velocidad de las animaciones:"

#: ui/main.qml:103
#, kde-format
msgctxt "Animation speed"
msgid "Slow"
msgstr "Lenta"

#: ui/main.qml:109
#, kde-format
msgctxt "Animation speed"
msgid "Instant"
msgstr "Instantánea"

#: ui/main.qml:123
#, kde-format
msgctxt ""
"part of a sentence: 'Clicking files or folders [opens them/selects them]'"
msgid "Clicking files or folders:"
msgstr "Al pulsar archivos o carpetas:"

#: ui/main.qml:130
#, kde-format
msgctxt "part of a sentence: 'Clicking files or folders selects them'"
msgid "Selects them"
msgstr "Se seleccionan"

#: ui/main.qml:144
#, kde-format
msgid "Open by double-clicking instead"
msgstr "Abrir con doble clic"

#: ui/main.qml:156
#, kde-format
msgctxt "part of a sentence: 'Clicking files or folders opens them'"
msgid "Opens them"
msgstr "Se abren"

#: ui/main.qml:169
#, kde-format
msgid "Select by clicking on item's selection marker"
msgstr "Seleccionar pulsando la marca de selección de los elementos"

#: ui/main.qml:184
#, kde-format
msgid "Clicking in scrollbar track:"
msgstr "Al pulsar en la barra de desplazamiento:"

#: ui/main.qml:185
#, kde-format
msgctxt ""
"@radio part of a complete sentence: 'Clicking in scrollbar track scrolls to "
"the clicked location'"
msgid "Scrolls to the clicked location"
msgstr "Desplazar hasta la posición en la que se ha pulsado"

#: ui/main.qml:203
#, kde-format
msgctxt ""
"@radio part of a complete sentence: 'Clicking in scrollbar track scrolls one "
"page up or down'"
msgid "Scrolls one page up or down"
msgstr "Desplazar una página arriba o abajo"

#: ui/main.qml:216
#, kde-format
msgid "Middle-click to scroll to clicked location"
msgstr "Clic central para desplazar hasta la posición en la que se ha pulsado"

#: ui/main.qml:229
#, kde-format
msgctxt ""
"@radio part of a complete sentence: 'Middle click pastes selected text'"
msgid "Middle Click:"
msgstr "Clic central:"

#: ui/main.qml:231
#, kde-format
msgctxt ""
"@radio part of a complete sentence: 'Middle click pastes selected text'"
msgid "Pastes selected text"
msgstr "Pega el texto seleccionado"

#: ui/main.qml:248
#, kde-format
msgid "Touch Mode:"
msgstr "Modo táctil:"

#: ui/main.qml:250
#, kde-format
msgctxt "As in: 'Touch Mode is automatically enabled as needed'"
msgid "Automatically enable as needed"
msgstr "Activar automáticamente cuando sea necesario"

#: ui/main.qml:250 ui/main.qml:293
#, kde-format
msgctxt "As in: 'Touch Mode is never enabled'"
msgid "Never enabled"
msgstr "Nunca activado"

#: ui/main.qml:266
#, kde-format
msgid ""
"Touch Mode will be automatically activated whenever the system detects a "
"touchscreen but no mouse or touchpad. For example: when a transformable "
"laptop's keyboard is flipped around or detached."
msgstr ""
"El modo táctil se activará automáticamente cada vez que el sistema detecte "
"una pantalla táctil sin ratón ni panel táctil. Por ejemplo: cuando el "
"teclado de un portátil transformable se pliega o se desprende."

#: ui/main.qml:271
#, kde-format
msgctxt "As in: 'Touch Mode is always enabled'"
msgid "Always enabled"
msgstr "Siempre activado"

#: ui/main.qml:311
#, kde-format
msgid ""
"In Touch Mode, many elements of the user interface will become larger to "
"more easily accommodate touch interaction."
msgstr ""
"En el modo táctil, muchos elementos de la interfaz de usuario se harán más "
"grandes para que la interacción táctil sea más fácil."

#: ui/main.qml:328
#, kde-format
msgid "Double-click interval:"
msgstr "Intervalo de doble clic:"

#: ui/main.qml:343
#, kde-format
msgid "%1 msec"
msgid_plural "%1 msec"
msgstr[0] "%1 ms"
msgstr[1] "%1 ms"

#: ui/main.qml:347
#, kde-format
msgid "msec"
msgstr "ms"

#: ui/main.qml:361
#, kde-format
msgid ""
"Two clicks within this duration are considered a double-click. Some "
"applications may not honor this setting."
msgstr ""
"Dos clics dentro de este intervalo de tiempo se consideran un doble clic. Es "
"posible que algunas aplicaciones no obedezcan este ajuste."

#. i18n: ectx: label, entry (singleClick), group (KDE)
#: workspaceoptions_kdeglobalssettings.kcfg:9
#, kde-format
msgid "Single click to open files"
msgstr "Un solo clic para abrir archivos"

#. i18n: ectx: label, entry (animationDurationFactor), group (KDE)
#: workspaceoptions_kdeglobalssettings.kcfg:13
#, kde-format
msgid "Animation speed"
msgstr "Velocidad de las animaciones"

#. i18n: ectx: label, entry (scrollbarLeftClickNavigatesByPage), group (KDE)
#: workspaceoptions_kdeglobalssettings.kcfg:17
#, kde-format
msgid "Left-click in scrollbar track moves scrollbar by one page"
msgstr "Un clic izquierdo en la barra de desplazamiento mueve una página"

#. i18n: ectx: label, entry (doubleClickInterval), group (KDE)
#: workspaceoptions_kdeglobalssettings.kcfg:21
#, kde-format
msgid "Double click interval"
msgstr "Intervalo de doble clic"

#. i18n: ectx: label, entry (tabletMode), group (Input)
#: workspaceoptions_kwinsettings.kcfg:9
#, kde-format
msgid "Automatically switch to touch-optimized mode"
msgstr "Cambiar automáticamente al modo táctil"

#. i18n: ectx: label, entry (primarySelection), group (Wayland)
#: workspaceoptions_kwinsettings.kcfg:15
#, kde-format
msgid "Enable middle click selection pasting"
msgstr "Activar el pegado de la selección con el clic central"

#. i18n: ectx: tooltip, entry (osdEnabled), group (OSD)
#: workspaceoptions_plasmasettings.kcfg:16
#, kde-format
msgid ""
"Show an on-screen display to indicate status changes such as brightness or "
"volume"
msgstr ""
"Mostrar un visor sobre la pantalla para cambios de estado, como del brillo o "
"del volumen."

#. i18n: ectx: label, entry (osdKbdLayoutChangedEnabled), group (OSD)
#: workspaceoptions_plasmasettings.kcfg:20
#, kde-format
msgid "OSD on layout change"
msgstr "OSD al cambiar la disposición"

#. i18n: ectx: tooltip, entry (osdKbdLayoutChangedEnabled), group (OSD)
#: workspaceoptions_plasmasettings.kcfg:21
#, kde-format
msgid "Show a popup on layout changes"
msgstr "Mostrar una ventana emergente cuando cambia la disposición"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Eloy Cuadra"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "ecuadra@eloihr.net"

#~ msgid "General Behavior"
#~ msgstr "Comportamiento general"

#~ msgid "System Settings module for configuring general workspace behavior."
#~ msgstr ""
#~ "Módulo de las preferencias del sistema para configurar el comportamiento "
#~ "general del espacio de trabajo."

#~ msgid "Furkan Tokac"
#~ msgstr "Furkan Tokac"

#~ msgid "Never optimize for touch usage"
#~ msgstr "No optimizar nunca para uso táctil"

#~ msgid "Always optimize for touch usage"
#~ msgstr "Optimizar siempre para uso táctil"

#~ msgid "Click behavior:"
#~ msgstr "Comportamiento del clic:"

#~ msgid "Double-click to open files and folders"
#~ msgstr "Doble clic para abrir archivos y carpetas"

#~ msgid "Select by single-clicking"
#~ msgstr "Seleccionar con un solo clic"

#~ msgid "Double-click to open files and folders (single click to select)"
#~ msgstr ""
#~ "Doble clic para abrir archivos y carpetas (un solo clic para seleccionar)"

#~ msgid "Plasma Workspace global options"
#~ msgstr "Opciones globales del espacio de trabajo Plasma"

#~ msgid "(c) 2009 Marco Martin"
#~ msgstr "© 2009 Marco Martin"

#~ msgid "Marco Martin"
#~ msgstr "Marco Martin"

#~ msgid "Maintainer"
#~ msgstr "Responsable"

#~ msgid "Show Informational Tips"
#~ msgstr "Mostrar consejos informativos"
